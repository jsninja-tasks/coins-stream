const io = require("socket.io-client");
const WebSocket = require("ws");
const http = require("http");

const socket = io.connect(
  "https://streamer.cryptocompare.com/",
  {
    reconnect: true
  }
);

const currentSubscriptions = [];
const websockets = {};

socket.on("connect", function(socket) {
  console.log("Connected!");
  currentSubscriptions.forEach(c =>
    socket.emit("SubAdd", { subs: [`2~Bitfinex~${c}~USD`] })
  );
});

function subscribe(currency) {
  const c = currency.toString().toUpperCase();
  if (currentSubscriptions.includes(c)) {
    console.log(currency, "already there");
    return;
  }
  currentSubscriptions.push(currency.toString().toUpperCase());
  socket.emit("SubAdd", { subs: [`2~Bitfinex~${c}~USD`] });
}

const server = http.createServer((req, res) => {
  res.end("OK");
});

const wss = new WebSocket.Server({ server });

wss.on("connection", function connection(ws) {
  ws.on("message", function incoming(message) {
    try {
      const m = JSON.parse(message);
      if (!m.currency) {
        throw new Error("Missing currency");
      }
      const currency = m.currency.toString().toUpperCase();
      if (m.type === "subscribe") {
        subscribe(currency);
        if (!Array.isArray(websockets[currency])) {
          websockets[currency] = [];
        }
        websockets[currency].push(ws);
      } else if (m.type === "unsubcribe") {
        if (!Array.isArray(websockets[currency])) {
          return;
        }
        websockets[currency].filter(i => i !== ws);
      }
    } catch (e) {
      ws.send(JSON.stringify({ error: e.message }));
    }
    console.log("received: %s", message);
  });
});

socket.on("m", function(message) {
  try {
    const [messageType, broker, from, to, priceMovement, price] = message.split(
      "~"
    );
    if (messageType === "3" || priceMovement === "4") {
      return;
    }

    const sockets = websockets[from] || [];
    const badSockets = [];
    sockets.forEach(s => {
      try {
        s.send(JSON.stringify({ currency: from, price }));
      } catch (e) {
        badSockets.push(s);
      }
    });
    sockets.filter(s => !badSockets.includes(s));
  } catch (e) {
    console.log("Error", e);
  }
});

server.listen(process.env.PORT || 3000);
